const { DataTypes } = require('sequelize');
const sequelize = require('./index').sequelize;

const Song = sequelize.define('Song', {
    title: DataTypes.STRING,
    artist: DataTypes.STRING,
    genre: DataTypes.STRING,
    album: DataTypes.STRING,
    albumImageUrl: DataTypes.STRING,
    youtubeID: DataTypes.STRING,
    lyrics: DataTypes.TEXT,
    tab: DataTypes.TEXT
})

// console.log(Song === sequelize.models.Song);

module.exports = Song;