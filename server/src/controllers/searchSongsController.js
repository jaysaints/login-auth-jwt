const Song = require('../models/Song.js')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports.search = async (search) => {
  console.log('Search ', search)
  var songs = null
  if (search) {
      songs = await Song.findAll({
          where: {
              [Op.or]: [
                  {
                    title: {
                      [Op.like]: `%${search}%`
                    }
                  },
                  {
                    genre: {
                      [Op.like]: `%${search}%`
                    }
                  },
                  {
                      artist: {
                        [Op.like]: `%${search}%`
                      }
                    },
                    {
                      album: {
                        [Op.like]: `%${search}%`
                      }
                    }
                ]
          }
        })
  } else {
      songs = await Song.findAll({
          limit: 10
      })
  }
  return songs
}
