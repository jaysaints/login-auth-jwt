const Song = require('../models/Song');
const querySong = require('../controllers/searchSongsController');

module.exports = {
    async registerSong_post (req, res, next) {
        try {
            const song = await Song.create(req.body)
            res.send({msg: 'Song registred!', song: song})
        } catch (error) {
            console.log(error)
            res.status(500).send({error: 'An erro has occured trying to create the songs!'})
        }
    },

    async getAllSongs_get (req, res, next) {
      try {
        const search = req.query.search
        if (search) {
            songs = await querySong.search(search)
        } else {
            songs = await Song.findAll({
                limit: 10
            })
            console.log('search empty')
        }
        res.send(songs)
      } catch (error) {
        res.status(500).send({error: 'An erro has occured try to fatch the songs!'})
      }
    },

    async getOneSong_get (req, res, next) {
        try {
            const song = await Song.findByPk(req.params.songId)
            if (!song) {
                res.status(500).send({success: false, error: 'This song was not found!'})
            }
            res.status(200).send(song)
        } catch (error) {
            res.status(500).send({success: true, error: 'This song was not found.'})
        }
    },

    async editSong_put (req, res, next) {
        try {
            const song = await Song.update(req.body, {
                where: {
                    id: req.params.songId
                }
            })
            res.send({msg: 'Song update!', song: req.body})
        } catch (error) {
            console.log(error)
            res.status(500).send({error: 'An erro has occured trying to update the song!'})
        }
    },
}
