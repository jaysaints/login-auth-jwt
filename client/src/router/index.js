import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index.vue'
import store from '../store/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "Login" */ '@/components/Register.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "Login" */ '@/components/Login.vue')
  },
  {
    path: '/songs',
    name: 'songs',
    component: () => import(/* webpackChunkName: "Login" */ '@/components/songs/Index.vue'),
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/songs/add',
    name: 'songs-register',
    component: () => import(/* webpackChunkName: "songs-register" */ '@/components/RegisterSongs.vue')
  },
  {
    path: '/songs/:songId',
    name: 'song',
    component: () => import(/* webpackChunkName: "song" */ '@/components/viewSong/Index.vue')
  },
  {
    path: '/songs/:songId/edit',
    name: 'song-edit',
    component: () => import(/* webpackChunkName: "edit" */ '@/components/EditSong.vue')
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  routes
})

// verify is authenticat
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      next('/login')
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next('/register')
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
